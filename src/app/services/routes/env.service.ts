import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root',
})
export class EnvService {

  // GET
  BASE_URL = 'https://cors-anywhere.herokuapp.com/https://komodo.kct.co.id/api';
  // POST
  LOGIN_URL = 'https://cors-anywhere.herokuapp.com/https://komodo.kct.co.id/api/kctauth/login';
  // GET
  LOGOUT_URL = 'https://cors-anywhere.herokuapp.com/https://komodo.kct.co.id/api/kctauth/logout';
  // GET
  HELPDESK_LIST = 'https://cors-anywhere.herokuapp.com/https://komodo.kct.co.id/api/helpdesk';
  // GET
  HELPDESK_SEARCH = 'https://cors-anywhere.herokuapp.com/https://komodo.kct.co.id/api/helpdesk?search=';
  // GET
  HELPDESK_LIST_FILTER = 'https://cors-anywhere.herokuapp.com/https://komodo.kct.co.id/api/helpdesk?filters=';

  constructor() { }
}
