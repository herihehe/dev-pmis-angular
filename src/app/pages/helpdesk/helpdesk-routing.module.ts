import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LayoutsModule } from 'src/app/layouts/layouts.module'

// dashboard
import { HelpdeskPageComponent } from 'src/app/pages/helpdesk/helpdesk.component'

const routes: Routes = [
  {
    path: 'list',
    component: HelpdeskPageComponent,
    data: { title: 'List' },
  },
]

@NgModule({
  imports: [LayoutsModule, RouterModule.forChild(routes)],
  providers: [],
  exports: [RouterModule],
})
export class HelpdeskRouterModule {}
