export const getMenuData: any[] = [
  {
    category: true,
    title: 'Helpdesk',
  },
  {
    title: 'Homepage',
    key: 'homepage',
    icon: 'fe fe-home',
    url: '/',
  },
  {
    title: 'Helpdesk',
    key: 'helpdesk',
    icon: 'fe fe-headphones',
    url: '/helpdesk/list',
  },
  {
    category: true,
    title: 'References',
  },
  {
    title: 'References',
    key: 'references',
    icon: 'fe fe-grid',
    childrenType: 'mega-menu',
    children: [
      {
        title: 'Title Menu 1',
        key: 'titleMenu1',
        icon: 'fe fe-airplay',
        url: '/',
      },{
        title: 'Title Menu 2',
        key: 'titleMenu2',
        icon: 'fe fe-archive',
        url: '/',
      },{
        title: 'Title Menu 3',
        key: 'titleMenu3',
        icon: 'fe fe-box',
        url: '/',
      },{
        title: 'Title Menu 4',
        key: 'titleMenu4',
        icon: 'fe fe-package',
        url: '/',
      },{
        title: 'Title Menu 5',
        key: 'titleMenu5',
        icon: 'fe fe-briefcase',
        url: '/',
      },{
        title: 'Title Menu 6',
        key: 'titleMenu6',
        icon: 'fe fe-airplay',
        url: '/',
      },{
        title: 'Title Menu 7',
        key: 'titleMenu7',
        icon: 'fe fe-archive',
        url: '/',
      },{
        title: 'Title Menu 8',
        key: 'titleMenu8',
        icon: 'fe fe-box',
        url: '/',
      },{
        title: 'Title Menu 9',
        key: 'titleMenu9',
        icon: 'fe fe-package',
        url: '/',
      },{
        title: 'Title Menu 10',
        key: 'titleMenu10',
        icon: 'fe fe-briefcase',
        url: '/',
      },{
        title: 'Title Menu 11',
        key: 'titleMenu11',
        icon: 'fe fe-airplay',
        url: '/',
      },{
        title: 'Title Menu 12',
        key: 'titleMenu12',
        icon: 'fe fe-archive',
        url: '/',
      },{
        title: 'Title Menu 13',
        key: 'titleMenu13',
        icon: 'fe fe-box',
        url: '/',
      },{
        title: 'Title Menu 14',
        key: 'titleMenu14',
        icon: 'fe fe-package',
        url: '/',
      },{
        title: 'Title Menu 15',
        key: 'titleMenu15',
        icon: 'fe fe-briefcase',
        url: '/',
      },{
        title: 'Title Menu 16',
        key: 'titleMenu16',
        icon: 'fe fe-airplay',
        url: '/',
      },{
        title: 'Title Menu 17',
        key: 'titleMenu17',
        icon: 'fe fe-archive',
        url: '/',
      },{
        title: 'Title Menu 18',
        key: 'titleMenu18',
        icon: 'fe fe-box',
        url: '/',
      },{
        title: 'Title Menu 19',
        key: 'titleMenu19',
        icon: 'fe fe-package',
        url: '/',
      },{
        title: 'Title Menu 20',
        key: 'titleMenu20',
        icon: 'fe fe-briefcase',
        url: '/',
      },{
        title: 'Title Menu 21',
        key: 'titleMenu21',
        icon: 'fe fe-airplay',
        url: '/',
      },{
        title: 'Title Menu 22',
        key: 'titleMenu22',
        icon: 'fe fe-archive',
        url: '/',
      },{
        title: 'Title Menu 23',
        key: 'titleMenu23',
        icon: 'fe fe-box',
        url: '/',
      },{
        title: 'Title Menu 24',
        key: 'titleMenu24',
        icon: 'fe fe-package',
        url: '/',
      },{
        title: 'Title Menu 25',
        key: 'titleMenu25',
        icon: 'fe fe-briefcase',
        url: '/',
      },
    ],
  },
  {
    category: true,
    title: 'Menu Fourth',
  },
  {
    title: 'Menu Fourth',
    key: 'menufourth',
    icon: 'fe fe-briefcase',
    url: '/'
  },
  {
    category: true,
    title: 'Other Menu',
  },
  {
    title: 'Other Menu',
    key: 'othermenus',
    icon: 'fe fe-database',
    children: [
      {
        title: 'Title Menu 1',
        key: 'titleMenu1',
        url: '/',
      },{
        title: 'Title Menu 2',
        key: 'titleMenu2',
        url: '/',
      },{
        title: 'Title Menu 3',
        key: 'titleMenu3',
        url: '/',
      },{
        title: 'Title Menu 4',
        key: 'titleMenu4',
        url: '/',
      },{
        title: 'Title Menu 5',
        key: 'titleMenu5',
        url: '/',
      }
    ],
  },
]
