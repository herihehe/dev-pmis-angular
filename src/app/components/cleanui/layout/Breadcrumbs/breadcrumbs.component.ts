import { Component, OnInit } from '@angular/core'
import { Router, NavigationStart } from '@angular/router'
import { Store } from '@ngrx/store'
import { filter } from 'rxjs/operators'
import { reduce } from 'lodash'
import { MenuService } from 'src/app/services/menu'
import { SearchServiceService } from 'src/app/services/search/search-service.service';
import store from 'store'

@Component({
  selector: 'cui-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss'],
})
export class BreadcrumbsComponent implements OnInit {

  menuData: any[]
  breadcrumbs: any[]
  inputValue: any[] = []
  val: string = ''
  col: string = ''
  filterSeparator = ': '

  constructor(
    private menuService: MenuService,
    private store: Store<any>,
    private router: Router,
    private globalSearchService: SearchServiceService,
  ) { }

  getSearchbarValues() {
    return this.inputValue.filter(value => !value.includes(this.filterSeparator))
  }

  getFilterValues() {
    return this.inputValue.filter(value => value.includes(this.filterSeparator))
  }

  getTitleMap(key) {
    const [label, value] = key.split(this.filterSeparator)
    const titleMap = {
      ticket_number: 'Ticket Number',
      status_name: 'Status',
      status__name: 'Status',
      name: 'Title',
      created_by_full_name: 'Requestor',
      receive_time: 'Receive Time',
      response_time: 'Response Time',
      resolution_time: 'Resolution Time',
    }
    return key.includes(this.filterSeparator) && titleMap[label] ? titleMap[label] + this.filterSeparator + value : key
  }

  async onInput(event: any) {
    this.inputValue.push(event.target.value)
    await store.set('searchbar_values', this.getSearchbarValues())
    this.globalSearchService.searchTerm.next(event.target.value)
    event.target.value = null
  }

  removeTag(tag, i): void {
    this.inputValue.splice(i, 1)
    if (tag.includes(this.filterSeparator)) {
      const [field] = tag.split(this.filterSeparator)
      const storedFilters = store.get('tableSearch_column')
      const newFilters = storedFilters.filter(filter => filter.field !== field)
      store.set('tableSearch_column', newFilters)
      this.globalSearchService.searchColumn.next({})
    } else {
      store.set('searchbar_values', this.getSearchbarValues())
      this.globalSearchService.searchTerm.next(null)
    }
  }

  ngOnInit() {
    this.menuService.getMenuData().subscribe(menuData => (this.menuData = menuData))
    this.generateBreadcrumbs(this.router.url)
    this.router.events
      .pipe(filter(event => event instanceof NavigationStart))
      .subscribe((event: NavigationStart) => {
        this.generateBreadcrumbs(event.url ? event.url : null)
      })

    this.globalSearchService.searchTerm.subscribe((newValue: string) => {
      if (newValue) {
        this.val = newValue
        this.tryJoinValues()
      }
    })

    this.globalSearchService.searchColumn.subscribe((param: {
      field: string,
      value: string
    }) => {
      if (param.field && param.value) {
        this.col = param.field
        this.val = param.value
        this.tryJoinValues()
      }
    })

    const storeData = store.get('searchbar_values')
    const storeDataFilter = store.get('tableSearch_column')
    if ((storeData || []).length) {
      this.inputValue = storeData
    }
    if ((storeDataFilter || []).length) {
      this.inputValue = [
        ...this.inputValue,
        ...storeDataFilter.map(filter => `${this.getTitleMap(filter.field)}${this.filterSeparator}${filter.value}`)
      ]
    }
  }

  tryJoinValues() {
    if (this.col && this.val) {
      const regex = new RegExp(`^${this.col}${this.filterSeparator}`)
      const filterId = this.inputValue.findIndex(value => value.match(regex))
      if (filterId > -1) {
        this.inputValue.splice(filterId, 1, this.col + this.filterSeparator + this.val)
      } else {
        this.inputValue.push(this.col + this.filterSeparator + this.val)
      }
      this.col = ''
      this.val = ''
    }
  }

  generateBreadcrumbs(event: any) {
    this.breadcrumbs = this.getPath(this.menuData, event).reverse()
  }

  getPath(data: any[], url: string, parents = []) {
    const items = reduce(
      data,
      (result: any, entry: any) => {
        if (result.length) {
          return result
        }
        if (entry.url === url) {
          return [entry].concat(parents)
        }
        if (entry.children) {
          const nested = this.getPath(entry.children, url, [entry].concat(parents))
          return (result || []).concat(nested.filter((e: any) => !!e))
        }
        return result
      },
      [],
    )
    return items.length > 0 ? items : [false]
  }
}
