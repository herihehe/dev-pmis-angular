import { Injectable } from '@angular/core';
import { EnvService } from '../routes/env.service';
import { tap } from 'rxjs/operators';
import { Observable, from } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import store from 'store'

@Injectable({
  providedIn: 'root'
})
export class HelpdeskService {

  token: any;

  constructor(
    private env: EnvService,
    private http: HttpClient,
  ) {
    this.token = store.get('accessToken');
  }

  // All
  getList(pageIndex): Observable<any> {
    const endpoint = pageIndex ? `${this.env.HELPDESK_LIST}?start=${pageIndex}&limit=10`: this.env.HELPDESK_LIST
    const tokenPromise = this.token = store.get('accessToken');

    return from(tokenPromise).pipe(
      switchMap((token) => {
        const httpOptions = {
          headers: new HttpHeaders({
            Accept: 'application/json, text/plain',
            'Content-Type': 'application/json',
            Authorization: 'Token ' + this.token,
          }),
        };
        return this.http
          .get(endpoint, httpOptions)
          .pipe(map((data) => data));
      })
    );
  }

  getRestOfTheList(limit,startFrom) {
    const tokenPromise =
    this.token === undefined
      ? store.get('accessToken')
      : Promise.resolve(this.token);

    return from(tokenPromise).pipe(
      switchMap((token) => {
        this.token = this.token;
        const httpOptions = {
          headers: new HttpHeaders({
            Accept: 'application/json, text/plain',
            'Content-Type': 'application/json',
            Authorization: 'Token '+this.token,
          }),
        };
        return this.http
        .get(`${this.env.HELPDESK_LIST}` + '?limit=' + `${limit}` + '&start=' + `${startFrom}`, httpOptions)
          .pipe(map((data) => data));
      })
    );
  }
}
