import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { EnvService } from 'src/app/services/routes/env.service'
import { map } from 'rxjs/operators'
import { HttpClient } from '@angular/common/http'
import store from 'store'

@Injectable({
  providedIn: 'root'
})
export class SearchServiceService {
  searchTerm = new BehaviorSubject<string>(null)
  searchColumn = new BehaviorSubject<object>({
    field: '',
    op: '',
    sort: '',
    value: ''
  })

  token: any;

  constructor(
    private env: EnvService,
    private http: HttpClient,
  ) {
    this.token = store.get('accessToken')
  }
  getSearchBar(searchParams) {
    const accessToken = store.get('accessToken')
    const storeData = store.get('tableSearch_column')
    const storeFilterParams  = (storeData || []).map(({ value, field, sort, op }) => {
      let column = field
      if (column === 'status_name') {
        column = 'status__name'
      }
      return `{ "value": "${value}", "field": "${column}", "sort": "${sort}", "op": "${op}" }`
    }).join(',')
    const filterParams = (storeData || []).length
      ? `&filters=[${storeFilterParams}]`
      : null
    const params = accessToken
      ? {
        headers: {
          Authorization: `Token ${accessToken}`,
          AccessToken: accessToken,
        },
      }
      : {}
    return this.http
      // tslint:disable-next-line:max-line-length
      .get(`${this.env.HELPDESK_SEARCH}[${searchParams.map(val => `"${val}"`).join(',')}]&fields=["ticket_number","name","status__name"]${filterParams || ''}`, params)
      .pipe(map((data) => data))
  }

  getTableFilter(filterParams) {
    const accessToken = store.get('accessToken')
    const storeData = store.get('searchbar_values')
    const searchParams = (storeData || []).length
      ? `&search=[${storeData.map(val => `"${val}"`).join(',')}]&fields=["ticket_number","name","status__name"]`
      : null
    const params = accessToken
      ? {
        headers: {
          Authorization: `Token ${accessToken}`,
          AccessToken: accessToken,
        },
      }
      : {}
    const normalizedFilterParams = filterParams.map(({ value, field, sort, op }) => {
      let column = field
      if (column === 'status_name') {
        column = 'status__name'
      }
      return `{ "value": "${value}", "field": "${column}", "sort": "${sort}", "op": "${op}" }`
    }).join(',')

    return this.http
      .get(`${this.env.HELPDESK_LIST_FILTER}[${normalizedFilterParams}]${searchParams || ''}`, params)
      .pipe(map((data) => data))
  }
}
