import { TestBed } from '@angular/core/testing';

import { MultireqService } from './multireq.service';

describe('MultireqService', () => {
  let service: MultireqService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MultireqService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
