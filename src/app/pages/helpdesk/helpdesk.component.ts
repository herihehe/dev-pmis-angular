import { Component, OnInit } from '@angular/core'
import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import { NzNotificationService } from 'ng-zorro-antd'
import { SortablejsOptions } from 'ngx-sortablejs'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { SearchServiceService } from 'src/app/services/search/search-service.service'
import { HelpdeskService } from 'src/app/services/services/helpdesk.service'
import { MultireqService } from 'src/app/services/services/multireq.service'
import store from 'store'

declare var require: any
// const data: any = require('./data.json')

interface DataItem {
  id: number;
  guid: string;
  ticket_number: string;
  status_name: string;
  name: string;
  created_by_full_name: string;
  receive_time: string;
  response_time: string;
  resolution_time: string;
}

interface ColumnItem {
  name: string;
  real_name: string;
  sortOrder: NzTableSortOrder | null;
  sortFn: NzTableSortFn | null;
  listOfFilter: NzTableFilterList;
  filterFn: NzTableFilterFn | null;
  filterMultiple: boolean;
  sortDirections: NzTableSortOrder[];
}

@Component({
  selector: 'app-helpdesk-list',
  templateUrl: './helpdesk.component.html',
  styleUrls: ['./helpdesk.component.scss']
})

export class HelpdeskPageComponent implements OnInit {
  layoutView: string = 'list';
  list: boolean = true;
  grid: boolean = false;
  kanban: boolean = false;
  reload: boolean = false;

  searchbar_values: boolean = false;
  tableSearch_column: boolean = false;
  visit_view_name: string = '';

  lineColors = ['bg-danger', 'bg-primary', 'bg-warning', 'bg-success'];

  options: SortablejsOptions = {
    group: 'agile-board',
  }

  // card colors
  badgeColors = {
    'Open': 'bg-primary',
    'In Progress': 'bg-warning',
    'Done': 'bg-success',
    'Backlog': 'bg-danger'
  }

  listDone: {
    loading: boolean,
    data: any[],
    page: number,
    total: number
  } = {
    loading: false,
    data: [],
    page: 0,
    total: 0,
  }
  listInProgress: {
    loading: boolean,
    data: any[],
    page: number,
    total: number
  } = {
    loading: false,
    data: [],
    page: 0,
    total: 0,
  }
  listOpen: {
    loading: boolean,
    data: any[],
    page: number,
    total: number
  } = {
    loading: false,
    data: [],
    page: 0,
    total: 0,
  }
  listBacklogs: {
    loading: boolean,
    data: any[],
    page: number,
    total: number
  } = {
    loading: false,
    data: [],
    page: 0,
    total: 0,
  }

  loading = false;
  filterForm: FormGroup
  listOfData: DataItem[] = []
  limit = ''
  page = ''
  pages = ''
  totalPages = ''
  results: any[] = []
  tickets: any[] = []
  status: any[] = []
  titles: any[] = []
  requestors: any[] = []
  receives: any[] = []
  responses: any[] = []
  resolutions: any[] = []


  done_status: DataItem[] = []
  progress_status: DataItem[] = []
  open_status: DataItem[] = []
  closed_status: DataItem[] = []

  done_results: any[] = []
  progress_results: any[] = []
  open_results: any[] = []
  closed_results: any[] = []

  onScrollIndex: number = 0

  listOfColumns: ColumnItem[] = [
    {
      name: 'Ticket Number',
      real_name: 'ticket_number',
      sortOrder: null,
      sortFn: (a: DataItem, b: DataItem) => a.ticket_number.length - b.ticket_number.length, //not working
      sortDirections: ['ascend', 'descend', null],
      filterMultiple: true,
      listOfFilter: this.tickets,
      filterFn: (name: string, item: DataItem) => item.ticket_number.indexOf(name) !== -1 // working
    },
    {
      name: 'Status',
      real_name: 'status_name',
      sortOrder: null,
      sortFn: (a: DataItem, b: DataItem) => a.status_name.length - b.status_name.length, // working
      sortDirections: ['ascend', 'descend', null],
      listOfFilter: this.status,
      filterFn: (name: string, item: DataItem) => item.status_name.indexOf(name) !== -1, // working
      filterMultiple: true
    },
    {
      name: 'Title',
      real_name: 'name',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: DataItem, b: DataItem) => a.name.length - b.name.length, // working
      filterMultiple: false,
      listOfFilter: this.titles,
      filterFn: (name: string, item: DataItem) => item.name.indexOf(name) !== -1 // working
    },
    {
      name: 'Requestor',
      real_name: 'created_by_full_name',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: DataItem, b: DataItem) => a.created_by_full_name.length - b.created_by_full_name.length, // working
      filterMultiple: false,
      listOfFilter: this.requestors,
      filterFn: (name: string, item: DataItem) => item.created_by_full_name.indexOf(name) !== -1, // working
    },
    {
      name: 'Receive Time',
      real_name: 'receive_time',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: DataItem, b: DataItem) => a.receive_time.length - b.receive_time.length, // working
      filterMultiple: false,
      listOfFilter: this.receives,
      filterFn: (name: string, item: DataItem) => item.receive_time.indexOf(name) !== -1, // working
    },
    {
      name: 'Response Time',
      real_name: 'response_time',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: DataItem, b: DataItem) => a.response_time.length - b.response_time.length, // working
      filterMultiple: false,
      listOfFilter: this.responses,
      filterFn: (name: string, item: DataItem) => item.response_time.indexOf(name) !== -1, // working
    },
    {
      name: 'Resolution Time',
      real_name: 'resolution_time',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: DataItem, b: DataItem) => a.resolution_time.length - b.resolution_time.length, // working (with null data it will return error)
      filterMultiple: false,
      listOfFilter: this.resolutions,
      filterFn: (name: string, item: DataItem) => item.resolution_time.indexOf(name) !== -1, // working (with null data it will return error)
    }
  ];

  isSpinning = true;
  searchTerm: string = "";

  displayDynamicColour(item) {
    return this.badgeColors[item];
  }

  constructor(
    private notification: NzNotificationService,
    public formBuilder: FormBuilder,
    private globalSearchService: SearchServiceService,
    private helpdeskService: HelpdeskService,
    private multireqService: MultireqService,
  ) {
    this.filterForm = this.formBuilder.group({
      formInput: [null, [Validators.required]],
      formSort: [null],
      formColumn: [null],
    })

    const getstoredata = store.get('searchbar_values')
    if (getstoredata) {
      this.searchbar_values = true;
      this.tableSearch_column = false;
    }
    const getstoredata2 = store.get('tableSearch_column')
    if (getstoredata2) {
      this.tableSearch_column = true;
      this.searchbar_values = false;
    }
    const getstoredata3 = store.get('visit_view_name')
    if (getstoredata3) {
      this.visit_view_name = getstoredata3.value;
      if (getstoredata3.value == 'list') {
        this.list = true;
        this.grid = false;
        this.kanban = false;
        this.reload = false;
        this.layoutView = 'list';
      } else if (getstoredata3.value == 'grid') {
        this.list = false;
        this.grid = true;
        this.kanban = false;
        this.reload = false;
        this.layoutView = 'grid';
      } else if (getstoredata3.value == 'kanban') {
        this.list = false;
        this.grid = false;
        this.kanban = true;
        this.reload = false;
        this.layoutView = 'kanban';
      }
    } else {
      this.visit_view_name = 'list';
    }
    this.changeLayoutView(getstoredata3.value)
  }

  ngOnInit(): void {
    if (!this.searchbar_values && !this.tableSearch_column) {
      this.getList(null)
    } else {
      this.filterOnLoad()
    }

    this.globalSearchService.searchTerm.subscribe(() => {
      this.searchData()
    })

    this.globalSearchService.searchColumn.subscribe(() => {
      this.filterData()
    })
  }

  searchData() {
    const storeData = store.get('searchbar_values') || []
    this.globalSearchService.getSearchBar(storeData || []).subscribe((data: any) => {
      this.listOfData = data.data
      this.limit = data.limit
      this.totalPages = data.total
      this.page = data.page
      this.pages = data.pages
      this.isSpinning = false
      this.searchbar_values = true
      if (this.tableSearch_column) {
        this.tableSearch_column = false
      }
    })
  }

  filterData() {
    const storeData = store.get('tableSearch_column') || []
    this.globalSearchService.getTableFilter(storeData || []).subscribe((data: any) => {
      this.listOfData = []
      this.listOfData = data.data
      this.limit = data.limit
      this.totalPages = data.total
      this.page = data.page
      this.pages = data.pages
      this.isSpinning = false
      this.tableSearch_column = true
      if (this.searchbar_values) {
        this.searchbar_values = false
      }
    })
  }

  filterOnLoad() {
    this.isSpinning = true
    this.searchData()
  }

  getList(pageIndex: number | null) {
    this.helpdeskService.getList(pageIndex).subscribe((res) => {
      this.listOfData = res.data;

      this.limit = res.limit //10
      this.totalPages = res.total
      this.page = res.page  //1
      this.pages = res.pages  //2

      for (let i = 0; i < (this.listOfData || []).length; i++) {
        this.tickets.push({
          text: res.data[i]['ticket_number'],
          value: res.data[i]['ticket_number'],
        })
        this.status.push({
          text: res.data[i]['status_name'],
          value: res.data[i]['status_name'],
        })
        this.titles.push({
          text: res.data[i]['name'],
          value: res.data[i]['name'],
        })
        this.requestors.push({
          text: res.data[i]['created_by_full_name'],
          value: res.data[i]['created_by_full_name'],
        })
        this.receives.push({
          text: res.data[i]['receive_time'],
          value: res.data[i]['receive_time'],
        })
        this.responses.push({
          text: res.data[i]['response_time'],
          value: res.data[i]['response_time'],
        })
        this.resolutions.push({
          text: res.data[i]['resolution_time'],
          value: res.data[i]['resolution_time'],
        })
      }

      this.isSpinning = this.listOfData !== null && this.listOfData.length <= 0;
    },
    (error) => {
      this.isSpinning = false
    });
  }

  onPageIndexChange(pageIndex) {
    this.getList(pageIndex)
  }

  // Table filters
  searchValue = ''
  visible = false

  search(column): void {
    const filterFormData = this.filterForm.value

    this.filterForm.controls.formInput.markAsDirty()
    this.filterForm.controls.formInput.updateValueAndValidity()
    if (this.filterForm.controls.formInput.invalid) {
      return
    }

    const storeData = store.get('tableSearch_column')
    const newFilter = {
      field: column,
      op: filterFormData.formColumn,
      sort: filterFormData.formSort,
      value: filterFormData.formInput
    }
    const filterId = (storeData || []).findIndex(filter => filter.field === column)
    const filterParams = [
      ...(storeData || []),
    ]
    if (filterId > -1) {
      filterParams.splice(filterId, 1, newFilter)
    } else {
      filterParams.push(newFilter)
    }
    store.set('tableSearch_column', filterParams)
    this.globalSearchService.searchColumn.next(newFilter)

    if (this.searchbar_values) {
      this.searchbar_values = false
    }

    this.isSpinning = true
    this.listOfData = []
    this.filterData()
  }

  async getDones(pageIndex: number | null) {
    const res = await this.multireqService.getRowDone(pageIndex)
    this.listDone.data = [
      ...this.listDone.data,
      ...res.data
    ]
    this.listDone.page = res.page
    this.listDone.total = res.total
    this.listDone.loading = false
  }

  async getInProgresses(pageIndex: number | null) {
    const res = await this.multireqService.getRowProgress(pageIndex)
    this.listInProgress.data = [
      ...this.listInProgress.data,
      ...res.data
    ]
    this.listInProgress.page = res.page
    this.listInProgress.total = res.total
    this.listInProgress.loading = false
  }

  async getOpens(pageIndex: number | null) {
    const res = await this.multireqService.getRowOpen(pageIndex)
    this.listOpen.data = [
      ...this.listOpen.data,
      ...res.data
    ]
    this.listOpen.page = res.page
    this.listOpen.total = res.total
    this.listOpen.loading = false
  }

  async getBacklogs(pageIndex: number | null) {
    const res = await this.multireqService.getRowBacklog(pageIndex)
    this.listBacklogs.data = [
      ...this.listBacklogs.data,
      ...res.data
    ]
    this.listBacklogs.page = res.page
    this.listBacklogs.total = res.total
    this.listBacklogs.loading = false
  }

  async onScrollDone() {
    if ((this.listDone.total === 0 ||
      this.listDone.data.length < this.listDone.total) &&
      !this.listDone.loading
    ) {
      this.listDone.loading = true
      await this.getDones(this.listDone.page)
    }
  }

  async onScrollProgresses() {
    if ((this.listInProgress.total === 0 ||
      this.listInProgress.data.length < this.listInProgress.total) &&
      !this.listInProgress.loading
    ) {
      this.listInProgress.loading = true
      await this.getInProgresses(this.listInProgress.page)
    }
  }

  async onScrollBacklog() {
    if ((this.listBacklogs.total === 0 ||
      this.listBacklogs.data.length < this.listBacklogs.total) &&
      !this.listBacklogs.loading
    ) {
      this.listBacklogs.loading = true
      await this.getBacklogs(this.listBacklogs.page)
    }
  }

  async onScrollOpen() {
    if ((this.listOpen.total === 0 ||
      this.listOpen.data.length < this.listOpen.total) &&
      !this.listOpen.loading
    ) {
      this.listOpen.loading = true
      await this.getOpens(this.listOpen.page)
    }
  }

  // checkbox
  listOfSelection = [
    {
      text: 'Select All Row',
      onSelect: () => {
        this.onAllChecked(true);
      }
    },
    {
      text: 'Select Odd Row',
      onSelect: () => {
        this.listOfCurrentPageData.forEach((data, index) => this.updateCheckedSet(data.guid, index % 2 !== 0));
        this.refreshCheckedStatus();
      }
    },
    {
      text: 'Select Even Row',
      onSelect: () => {
        this.listOfCurrentPageData.forEach((data, index) => this.updateCheckedSet(data.guid, index % 2 === 0));
        this.refreshCheckedStatus();
      }
    }
  ];
  checked = false;
  indeterminate = false;
  listOfCurrentPageData: DataItem[] = [];
  setOfCheckedId = new Set<string>();

  updateCheckedSet(id: string, checked: boolean): void {
    if (checked) {
      this.setOfCheckedId.add(id);
      this.notification.success('Checked', 'You have checked rows numebr ' + id + '!')
    } else {
      this.setOfCheckedId.delete(id);
      this.notification.warning('Checked', 'You have unchecked rows numebr ' + id + '!')
    }
  }

  onItemChecked(id: string, checked: boolean): void {
    this.updateCheckedSet(id, checked);
    this.refreshCheckedStatus();
  }

  onAllChecked(value: boolean): void {
    this.listOfCurrentPageData.forEach(item => this.updateCheckedSet(item.guid, value));
    this.refreshCheckedStatus();
  }

  onCurrentPageDataChange($event: DataItem[]): void {
    this.listOfCurrentPageData = $event;
    this.refreshCheckedStatus();
  }

  refreshCheckedStatus(): void {
    this.checked = this.listOfCurrentPageData.every(item => this.setOfCheckedId.has(item.guid));
    this.indeterminate = this.listOfCurrentPageData.some(item => this.setOfCheckedId.has(item.guid)) && !this.checked;
  }

  //markerClick
  markerClick(ticket) {
    this.notification.success('Clicked', 'You have clicked row with ticket number ' + ticket + '!')
  }

  // buttons notifications
  buttonClick(data) {
    this.notification.success('Clicked', 'You have ' + data + '!')
  }

  // change layout
  changeLayoutView(val) {
    if (val == 'list') {
      store.set('visit_view_name', { value: 'list' });
      this.list = true;
      this.grid = false;
      this.kanban = false;
      this.reload = false;
      this.layoutView = 'list';
    } else if (val == 'grid') {
      store.set('visit_view_name', { value: 'grid' });
      this.done_status = []
      this.progress_status = []
      this.open_status = []
      this.closed_status = []
      this.done_results = []
      this.progress_results = []
      this.open_results = []
      this.closed_results = []
      this.onScrollDone()
      this.onScrollProgresses()
      this.onScrollOpen()
      this.onScrollBacklog()
      this.list = false;
      this.grid = true;
      this.kanban = false;
      this.reload = false;
      this.layoutView = 'grid';
    } else if (val == 'kanban') {
      store.set('visit_view_name', { value: 'kanban' });
      this.done_status = []
      this.progress_status = []
      this.open_status = []
      this.closed_status = []
      this.done_results = []
      this.progress_results = []
      this.open_results = []
      this.closed_results = []
      this.onScrollDone()
      this.onScrollProgresses()
      this.onScrollOpen()
      this.onScrollBacklog()
      this.list = false;
      this.grid = false;
      this.kanban = true;
      this.reload = false;
      this.layoutView = 'kanban';
    } else if (val == 'reload') {
      store.remove('tableSearch_column')
      store.remove('searchbar_values')
      this.isSpinning = true;
      this.listOfData = [];
      this.results = [];
      this.getList(null);
      this.list = this.list;
      this.grid = this.grid;
      this.kanban = this.kanban;
      this.reload = this.reload;
      this.layoutView = this.layoutView;
    }
  }
}
