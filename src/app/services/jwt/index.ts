import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { EnvService } from '../routes/env.service'
import store from 'store'

@Injectable()
export class jwtAuthService {
  constructor(
    private http: HttpClient,
    private env: EnvService,
  ) {}

  login(username: string, password: string): Observable<any> {
    return this.http.post(`${this.env.LOGIN_URL}`, { username, password })
  }

  register(email: string, password: string, name: string): Observable<any> {
    return this.http.post('/api/auth/register', { email, password, name })
  }

  currentAccount(): Observable<any> {
    const accessToken = store.get('accessToken')
    const params = accessToken
      ? {
          headers: {
            Authorization: `Bearer ${accessToken}`,
            AccessToken: accessToken,
          },
        }
      : {}
    return this.http.get('/api/auth/account', params)
  }

  logout(): Observable<any> {
    // return this.http.get(`${this.env.LOGOUT_URL}`)
    return this.http.get('/api/auth/logout')
  }
}
