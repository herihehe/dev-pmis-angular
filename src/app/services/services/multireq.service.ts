import { Injectable } from '@angular/core'
import { EnvService } from '../routes/env.service'
import { from, Observable } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import store from 'store'

@Injectable({
  providedIn: 'root'
})
export class MultireqService {
  token: any

  constructor(
    private env: EnvService,
    private http: HttpClient,
  ) {
    this.token = store.get('accessToken')
  }

  // Get each row data with different status name
  getRowDone(pageIndex: number): any {
    const endpoint = pageIndex ? `${this.env.HELPDESK_SEARCH}["done"]&fields=["status__name"]&start=${pageIndex}` : `${this.env.HELPDESK_SEARCH}["done"]&fields=["status__name"]&start=0`
    const token = store.get('accessToken')
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json, text/plain',
        'Content-Type': 'application/json',
        Authorization: 'Token ' + token,
      }),
    }

    return new Promise((resolve, reject) => {
      this.http
        .get(endpoint, httpOptions)
        .toPromise()
        .then(resolve, reject)
    })
  }
  getRowProgress(pageIndex: number): any {
    const endpoint = pageIndex ? `${this.env.HELPDESK_SEARCH}["in progress"]&fields=["status__name"]&start=${pageIndex}` : `${this.env.HELPDESK_SEARCH}["in progress"]&fields=["status__name"]&start=0`
    const token = store.get('accessToken')
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json, text/plain',
        'Content-Type': 'application/json',
        Authorization: 'Token ' + token,
      }),
    }

    return new Promise((resolve, reject) => {
      this.http
        .get(endpoint, httpOptions)
        .toPromise()
        .then(resolve, reject)
    })
  }
  getRowOpen(pageIndex: number): any {
    const endpoint = pageIndex ? `${this.env.HELPDESK_SEARCH}["open"]&fields=["status__name"]&start=${pageIndex}` : `${this.env.HELPDESK_SEARCH}["open"]&fields=["status__name"]&start=0`
    const token = store.get('accessToken')
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json, text/plain',
        'Content-Type': 'application/json',
        Authorization: 'Token ' + token,
      }),
    }

    return new Promise((resolve, reject) => {
      this.http
        .get(endpoint, httpOptions)
        .toPromise()
        .then(resolve, reject)
    })
  }
  getRowBacklog(pageIndex: number): any {
    const endpoint = pageIndex ? `${this.env.HELPDESK_SEARCH}["backlog"]&fields=["status__name"]&start=${pageIndex}` : `${this.env.HELPDESK_SEARCH}["backlog"]&fields=["status__name"]&start=0`
    const token = store.get('accessToken')
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json, text/plain',
        'Content-Type': 'application/json',
        Authorization: 'Token ' + token,
      }),
    }

    return new Promise((resolve, reject) => {
      this.http
        .get(endpoint, httpOptions)
        .toPromise()
        .then(resolve, reject)
    })
  }
}
