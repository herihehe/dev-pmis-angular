import { NgModule } from '@angular/core'
import { SharedModule } from 'src/app/shared.module'
import { HelpdeskRouterModule } from './helpdesk-routing.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'

// dashboard
import { HelpdeskPageComponent } from 'src/app/pages/helpdesk/helpdesk.component'
import { SortablejsModule } from 'ngx-sortablejs'

const COMPONENTS = [
  HelpdeskPageComponent,
]

@NgModule({
  imports: [SharedModule, FormsModule, HelpdeskRouterModule, ReactiveFormsModule, CommonModule, SortablejsModule],
  declarations: [...COMPONENTS],
})
export class HelpdeskModule {}
